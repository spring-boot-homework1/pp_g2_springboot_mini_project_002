package com.springmvc.demo.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadServiceInterface {
    String fileUpload(MultipartFile multipartFile);
}
