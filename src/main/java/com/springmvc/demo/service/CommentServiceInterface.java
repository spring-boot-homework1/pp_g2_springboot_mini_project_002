package com.springmvc.demo.service;

import com.springmvc.demo.model.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentServiceInterface {

    List<Comment> fetchAllCommentInPosted(int post_id);
    void insertCommentToPosted(String comment, int post_id);
}
