You can find our database data and structure file which is name "mybatis.sql"

or

Run this database script at on your database:
=============================================================================

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE sub_reddit(
    id SERIAL PRIMARY KEY NOT NULL ,
    title VARCHAR(255)
);

CREATE TABLE post(
    id SERIAL PRIMARY KEY NOT NULL ,
    title VARCHAR(255) NOT NULL ,
    description TEXT ,
    image_url TEXT,
    sub_reddit_id INTEGER REFERENCES sub_reddit ON DELETE CASCADE ON UPDATE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    vote INTEGER DEFAULT 0,
    FOREIGN KEY (sub_reddit_id) REFERENCES sub_reddit(id)
);

CREATE TABLE comment(
    id SERIAL PRIMARY KEY NOT NULL,
    comment TEXT,
    post_id INTEGER REFERENCES post ON DELETE CASCADE ON UPDATE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    vote INTEGER DEFAULT 0,
    FOREIGN KEY (post_id) REFERENCES post(id)
);

INSERT INTO sub_reddit (id, title) VALUES(1, 'Information Technology');
INSERT INTO sub_reddit (id, title) VALUES(2, 'Society');
INSERT INTO sub_reddit (id, title) VALUES(3, 'History');

INSERT INTO post (id, title, description, image_url, sub_reddit_id, vote) VALUES (1, 'Spring Boot', 'The Spring Framework is an application framework and inversion of control container for the Java platform. The framework''s core features can be used by any Java application, but there are extensions for building web applications on top of the Java EE platform.', '2a3a93ec-acb9-4a95-ba30-4588e0164307.gif', 1, 0);
INSERT INTO post (id, title, description, image_url, sub_reddit_id, vote) VALUES (2, 'Node Js', 'Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser.', '0e8ba0cd-c0c3-4885-a3a9-e290035ceaa7.gif', 1, 0);
INSERT INTO post (id, title, description, image_url, sub_reddit_id, vote) VALUES (3, 'JavaScript', 'JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.', '8e9cda40-8680-4942-be67-d1e9a2609114.gif', 1, 0);