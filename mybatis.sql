/*
 Navicat Premium Data Transfer

 Source Server         : postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : localhost:5432
 Source Catalog        : mybatis002
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 03/06/2021 22:48:00
*/


-- ----------------------------
-- Sequence structure for comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comment_id_seq";
CREATE SEQUENCE "public"."comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_id_seq";
CREATE SEQUENCE "public"."post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sub_reddit_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sub_reddit_id_seq";
CREATE SEQUENCE "public"."sub_reddit_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS "public"."comment";
CREATE TABLE "public"."comment" (
  "id" int4 NOT NULL DEFAULT nextval('comment_id_seq'::regclass),
  "comment" text COLLATE "pg_catalog"."default",
  "post_id" int4,
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "vote" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS "public"."post";
CREATE TABLE "public"."post" (
  "id" int4 NOT NULL DEFAULT nextval('post_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "image_url" text COLLATE "pg_catalog"."default",
  "sub_reddit_id" int4,
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "vote" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO post (title, description, image_url, sub_reddit_id, vote) VALUES ('Spring Boot', 'The Spring Framework is an application framework and inversion of control container for the Java platform. The framework''s core features can be used by any Java application, but there are extensions for building web applications on top of the Java EE platform.', '2a3a93ec-acb9-4a95-ba30-4588e0164307.gif', 1, 0);
INSERT INTO post (title, description, image_url, sub_reddit_id, vote) VALUES ('Node Js', 'Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser.', '0e8ba0cd-c0c3-4885-a3a9-e290035ceaa7.gif', 1, 0);
INSERT INTO post (title, description, image_url, sub_reddit_id, vote) VALUES ('JavaScript', 'JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.', '8e9cda40-8680-4942-be67-d1e9a2609114.gif', 1, 0);

-- ----------------------------
-- Table structure for sub_reddit
-- ----------------------------
DROP TABLE IF EXISTS "public"."sub_reddit";
CREATE TABLE "public"."sub_reddit" (
  "id" int4 NOT NULL DEFAULT nextval('sub_reddit_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of sub_reddit
-- ----------------------------
INSERT INTO sub_reddit (title) VALUES('Information Technology');
INSERT INTO sub_reddit (title) VALUES('Society');
INSERT INTO sub_reddit (title) VALUES('History');

-- ----------------------------
-- Function structure for trigger_set_timestamp
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."trigger_set_timestamp"();
CREATE OR REPLACE FUNCTION "public"."trigger_set_timestamp"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."comment_id_seq"
OWNED BY "public"."comment"."id";
SELECT setval('"public"."comment_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."post_id_seq"
OWNED BY "public"."post"."id";
SELECT setval('"public"."post_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sub_reddit_id_seq"
OWNED BY "public"."sub_reddit"."id";
SELECT setval('"public"."sub_reddit_id_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table comment
-- ----------------------------
ALTER TABLE "public"."comment" ADD CONSTRAINT "comment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "post_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sub_reddit
-- ----------------------------
ALTER TABLE "public"."sub_reddit" ADD CONSTRAINT "sub_reddit_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table comment
-- ----------------------------
ALTER TABLE "public"."comment" ADD CONSTRAINT "comment_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."post" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."comment" ADD CONSTRAINT "comment_post_id_fkey1" FOREIGN KEY ("post_id") REFERENCES "public"."post" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "post_sub_reddit_id_fkey" FOREIGN KEY ("sub_reddit_id") REFERENCES "public"."sub_reddit" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."post" ADD CONSTRAINT "post_sub_reddit_id_fkey1" FOREIGN KEY ("sub_reddit_id") REFERENCES "public"."sub_reddit" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
