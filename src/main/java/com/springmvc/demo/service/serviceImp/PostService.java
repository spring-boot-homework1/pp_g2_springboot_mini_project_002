package com.springmvc.demo.service.serviceImp;

import com.springmvc.demo.model.Post;
import com.springmvc.demo.repository.PostRepositoryInterface;
import com.springmvc.demo.service.PostServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PostService implements PostServiceInterface {

    private PostRepositoryInterface postRepository;

    @Autowired
    public void setPostRepository(PostRepositoryInterface postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> fetchAllPost() {
        return postRepository.fetchAllPost();
    }

    @Override
    public Post findPostById(int id) {
        return postRepository.findPostById(id);
    }

    @Override
    public void deletePosted(int id) {
        postRepository.deletePosted(id);
    }

    @Override
    public void insertPost(String title, String description, String image_url, int sub_reddit_id) {
        postRepository.insertPost(title, description, image_url, sub_reddit_id);
    }

    @Override
    public void updatePosted(String title, String description, String image_url, int sub_reddit_id) {
        postRepository.updatePosted(title, description, image_url, sub_reddit_id);
    }

    @Override
    public void upVote(int id) {
        postRepository.upVote(id);
    }

    @Override
    public void downVote(int id) {
        postRepository.downVote(id);
    }
}
