package com.springmvc.demo.service.serviceImp;

import com.springmvc.demo.model.Comment;
import com.springmvc.demo.repository.CommentRepository;
import com.springmvc.demo.service.CommentServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements CommentServiceInterface {

    private CommentRepository commentRepository;

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> fetchAllCommentInPosted(int post_id) {
        return commentRepository.fetchAllCommentInPosted(post_id);
    }

    @Override
    public void insertCommentToPosted(String comment, int post_id) {
        commentRepository.addCommentToPosted(comment, post_id);
    }
}
