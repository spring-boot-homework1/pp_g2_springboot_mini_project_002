package com.springmvc.demo.service;

import com.springmvc.demo.model.Post;
import com.springmvc.demo.repository.PostRepositoryInterface;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface PostServiceInterface {
    List<Post> fetchAllPost();
    Post findPostById(int id);
    void deletePosted(int id);
    void insertPost(String title, String description, String image_url, int sub_reddit_id);
    void updatePosted(String title, String description, String image_url, int sub_reddit_id);
    void upVote(int id);
    void downVote(int id);
}
