package com.springmvc.demo.service;

import com.springmvc.demo.model.Subreddit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SubredditServiceInterface {
    List<Subreddit> fetchAllSubreddit();
    Subreddit findSubredditById(int id);
    void insertSubReddit(String title);
}
