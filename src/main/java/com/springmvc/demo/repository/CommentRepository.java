package com.springmvc.demo.repository;

import com.springmvc.demo.model.Comment;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CommentRepository {

    @Select("SELECT * FROM comment WHERE post_id = #{post_id}")
    List<Comment> fetchAllCommentInPosted(int post_id);

    @Insert("INSERT INTO comment (comment, post_id) VALUES (#{comment}, #{post_id})")
    void addCommentToPosted(String comment, int post_id);
}
