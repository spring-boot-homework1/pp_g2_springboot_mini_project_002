package com.springmvc.demo.controller;

import com.springmvc.demo.model.Comment;
import com.springmvc.demo.service.serviceImp.CommentService;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CommentController {
    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/view/details/comment")
    public String insertComment(@ModelAttribute Comment comment, @RequestParam int post_id){
        commentService.insertCommentToPosted(comment.getComment(), post_id);
        return "redirect:/view/details?id="+post_id;
    }
}
