package com.springmvc.demo.controller;

import com.springmvc.demo.model.Comment;
import com.springmvc.demo.model.Post;
import com.springmvc.demo.model.Subreddit;
import com.springmvc.demo.service.serviceImp.CommentService;
import com.springmvc.demo.service.serviceImp.PostService;
import com.springmvc.demo.service.serviceImp.FileUploadService;
import com.springmvc.demo.service.serviceImp.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class PostController {
    private PostService postService;
    private FileUploadService fileUploadService;
    private SubredditService subredditService;
    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setSubredditService(SubredditService subredditService) {
        this.subredditService = subredditService;
    }

    @Autowired
    public void setFileUploadService(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @Value("${file.imageUrl}")
    String imgUrl;

    @GetMapping("/")
    public String homePage(Model model){
        List<Subreddit> subredditList = subredditService.fetchAllSubreddit();
        model.addAttribute("subredditList", subredditList);
        List<Post> posts = postService.fetchAllPost();
        model.addAttribute("posts", posts);
        return "index";
    }

    @RequestMapping("/findById")
    public Post findById(@PathVariable int id) {
        return postService.findPostById(id);
    }

    @RequestMapping(value="/delete", method = {RequestMethod.DELETE, RequestMethod.GET})
    public String delete(int id) {
        postService.deletePosted(id);
        return "redirect:/";
    }

    @PostMapping("/create-post")
    public String insertPost(@ModelAttribute Post posted, @RequestPart("file") MultipartFile file){
        String fileName = fileUploadService.fileUpload(file);
        posted.setImage_url(fileName);
        postService.insertPost(posted.getTitle(), posted.getDescription(), posted.getImage_url(), posted.getSub_reddit_id());
        return "redirect:/";
    }

    @GetMapping("/view/details")
    public String viewPosted(Model model, @RequestParam int id){
        Post post = postService.findPostById(id);
        List<Comment> comments = commentService.fetchAllCommentInPosted(id);
        model.addAttribute("post", post);
        model.addAttribute("comments", comments);
        return "view";
    }

    @GetMapping("/upvote")
    public String upVote(@RequestParam int id){
        postService.upVote(id);
        return "redirect:/";
    }

    @GetMapping("/downvote")
    public String downVote(@RequestParam int id){
        postService.downVote(id);
        return "redirect:/";
    }

    @GetMapping("/view/details/upvote")
    public String upVoteView(@RequestParam int id){
        postService.upVote(id);
        return "redirect:/view/details?id="+id;
    }

    @GetMapping("/view/details/downvote")
    public String downVoteView(@RequestParam int id){
        postService.downVote(id);
        return "redirect:/view/details?id="+id;
    }


}
