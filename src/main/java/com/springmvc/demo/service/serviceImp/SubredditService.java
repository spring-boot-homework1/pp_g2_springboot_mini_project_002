package com.springmvc.demo.service.serviceImp;

import com.springmvc.demo.model.Subreddit;
import com.springmvc.demo.repository.SubredditRepository;
import com.springmvc.demo.service.SubredditServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubredditService implements SubredditServiceInterface {

    private SubredditRepository subredditRepository;

    @Autowired
    public void setSubredditRepository(SubredditRepository subredditRepository) {
        this.subredditRepository = subredditRepository;
    }

    @Override
    public List<Subreddit> fetchAllSubreddit() {
        return subredditRepository.fetchAllSubreddit();
    }

    @Override
    public Subreddit findSubredditById(int id) {
        return subredditRepository.findSubredditById(id);
    }

    @Override
    public void insertSubReddit(String title) {
        subredditRepository.InsertSubReddit(title);
    }
}
