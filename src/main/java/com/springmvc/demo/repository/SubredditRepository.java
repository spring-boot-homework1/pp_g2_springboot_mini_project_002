package com.springmvc.demo.repository;

import com.springmvc.demo.model.Subreddit;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SubredditRepository {

    @Select("SELECT * FROM sub_reddit")
    List<Subreddit> fetchAllSubreddit();

    @Select("SELECT * FROM sub_reddit WHERE id = #{id}")
    Subreddit findSubredditById(int id);

    @Insert("INSERT INTO sub_reddit (title) VALUES (#{title})")
    void InsertSubReddit(String title);
}
