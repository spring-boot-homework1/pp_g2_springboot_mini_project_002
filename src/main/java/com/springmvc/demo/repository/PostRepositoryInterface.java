package com.springmvc.demo.repository;

import com.springmvc.demo.model.Post;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

@Mapper
public interface PostRepositoryInterface {
    @Select("SELECT * FROM post ORDER BY id ASC")
    List<Post> fetchAllPost();

    @Select("SELECT * FROM post WHERE id = #{id}")
    Post findPostById(int id);

    @Delete("DELETE FROM post WHERE id = #{id}")
    void deletePosted(int id);

    @Insert("INSERT INTO post (title, description, image_url, sub_reddit_id) " +
            "VALUES (#{title}, #{description}, #{image_url}, #{sub_reddit_id})")
    void insertPost(String title, String description, String image_url, int sub_reddit_id);

    @Update("UPDATE post SET " +
            "title = #{title}, description = #{description}" +
            "image_url = #{image_url}, sub_reddit_id = #{sub_reddit_id}" +
            "WHERE id = #{id}")
    void updatePosted(String title, String description, String image_url, int sub_reddit_id);

    @Update("UPDATE post SET vote = vote + 1 WHERE id = #{id}")
    void upVote(int id);

    @Update("UPDATE post SET vote = vote - 1 WHERE id = #{id}")
    void downVote(int id);
}
