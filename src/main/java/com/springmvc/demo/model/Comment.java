package com.springmvc.demo.model;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    private int id;
    private String comment;
    private int post_id;
    private String created_at;
}
