package com.springmvc.demo.controller;

import com.springmvc.demo.model.Post;
import com.springmvc.demo.model.Subreddit;
import com.springmvc.demo.service.serviceImp.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class SubredditController {

    private SubredditService subredditService;

    @Autowired
    public void setSubredditService(SubredditService subredditService) {
        this.subredditService = subredditService;
    }

    @PostMapping("/create-subreddit")
    public String insertPost(@ModelAttribute Subreddit subreddit){
        subredditService.insertSubReddit(subreddit.getTitle());
        return "redirect:/";
    }
}
