package com.springmvc.demo.model;

import lombok.experimental.Accessors;

import java.util.Date;

@Accessors(chain = true)
public class Post {
    private int id;
    private String title;
    private String description;
    private String image_url;
    private Date created_at;
    private int sub_reddit_id;
    private int vote;

    public Post() {
    }

    public Post(int id, String title, String description, String image_url, Date created_at, int sub_reddit_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image_url = image_url;
        this.created_at = created_at;
        this.sub_reddit_id = sub_reddit_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getSub_reddit_id() {
        return sub_reddit_id;
    }

    public void setSub_reddit_id(int sub_reddit_id) {
        this.sub_reddit_id = sub_reddit_id;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }
}
